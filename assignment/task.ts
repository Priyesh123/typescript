class Person{
    name : string;
    age : number;
    salary : number;
    sex : string;
    constructor(name: string,age: number,salary: number,sex: string)
    {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }

    static sorted(Personarr: Person[],fieldName: string,Order : string): Person[]{
        let result : Person[];
        result = [];
        if(fieldName === 'name'){
            if(Order === 'asc'){
                result = this.quickSortAscOnName(Personarr);
            }
            else{
                result = this.quickSortDescOnName(Personarr);
            }
        }
        else if(fieldName === 'age'){
            if(Order === 'asc'){
                result = this.quickSortAscOnAge(Personarr);
            }
            else{
                result = this.quickSortDescOnAge(Personarr);
            }
        }
        else if(fieldName === 'salary'){
            if(Order === 'asc'){
                result = this.quickSortAscOnSalary(Personarr);
            }
            else{
                result = this.quickSortDescOnSalary(Personarr);
            }
        }
        else{
            if(Order === 'asc'){
                // female first
                result = this.quickSortAscOnGender(Personarr);
            }
            else{
                // male first
                result = this.quickSortDescOnGender(Personarr);
            }
        }

        console.log(result);
        return result;
    }

    static quickSortAscOnName(arr: Person[]): Person[]{
        if(arr.length <= 1)
        {
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let pivot : Person; pivot = arr.pop();
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].name.localeCompare(pivot.name) <= 0) {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...newArray,...this.quickSortAscOnName(left),pivot,...this.quickSortAscOnName(right)];
    }  

    static quickSortDescOnName(arr: Person[]): Person[]{
        if(arr.length <= 1)
        {
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let pivot : Person; pivot = arr.pop();
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].name.localeCompare(pivot.name) >= 0) {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...newArray,...this.quickSortDescOnName(left),pivot,...this.quickSortDescOnName(right)];
    }

    static quickSortAscOnAge(arr: Person[]): Person[]{
        if(arr.length <= 1)
        {
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let pivot : Person; pivot = arr.pop();
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].age <= pivot.age) {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...newArray,...this.quickSortAscOnAge(left),pivot,...this.quickSortAscOnAge(right)];
    }

    static quickSortDescOnAge(arr: Person[]): Person[]{
        if(arr.length <= 1)
        {
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let pivot : Person; pivot = arr.pop();
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].age > pivot.age) {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...newArray,...this.quickSortDescOnAge(left),pivot,...this.quickSortDescOnAge(right)];
    }

    static quickSortAscOnSalary(arr: Person[]): Person[]{
        if(arr.length <= 1)
        {
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let pivot : Person; pivot = arr.pop();
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].salary <= pivot.salary) {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...newArray,...this.quickSortAscOnSalary(left),pivot,...this.quickSortAscOnSalary(right)];
    }

    static quickSortDescOnSalary(arr: Person[]): Person[]{
        if(arr.length <= 1){
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let pivot : Person; pivot = arr.pop();
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].salary > pivot.salary) {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...newArray,...this.quickSortDescOnSalary(left),pivot,...this.quickSortDescOnSalary(right)];
    }


    static quickSortAscOnGender(arr : Person[]): Person[]{
        if(arr.length <= 1){
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].sex === 'female') {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...left,...right];
    }

    static quickSortDescOnGender(arr : Person[]): Person[]{
        if(arr.length <= 1){
            return arr;
        }
        let left : Person[]; left = [];
        let right : Person[]; right = [];
        let newArray : Person[]; newArray = [];
        let length : number; length = arr.length;
        let i : number;
        for(i = 0; i < length;i++){
            if (arr[i].sex === 'male') {
                left.push(arr[i]);
            } else {
                right.push(arr[i]);
            }
        }
        return newArray = [...left,...right];
    }
}



let Parr : Person[];
Parr = [];
Parr.push(new Person("Naman",23,70,"male"));
Parr.push(new Person("Riya",22,900,"female"));
Parr.push(new Person("Navneet",26,7500,"male"));
Parr.push(new Person("Naved",29,79000,"male"));
Parr.push(new Person("Jasmine",20,35000,"female"));
Parr.push(new Person("Anamika",19,40000,"female"));
Parr.push(new Person("Anshika",28,7000,"female"));
Parr.push(new Person("Ayush",31,90,"male"));
Parr.push(new Person("Ritk",34,70008,"male"));
Parr.push(new Person("Avnesh",35,129412,"male"));
Parr.push(new Person("Nivedita",40,11,"female"));
Parr.push(new Person("Mayank",43,91000,"male"));
Parr.push(new Person("Manish",45,87352000,"male"));
console.log(Parr);
let ans : Person[];
ans = [];
ans = Person.sorted(Parr,'gender','desc');
console.log(ans);