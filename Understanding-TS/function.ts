function add(n1: number,n2: number): number{
    return n1 + n2;
}

// import e from "cors";


function PrintResult(num : number){
    // No return statement hence the return type here is void.
    console.log(num);
}

// // void ko print karenge toh undefined aayega because in JS there is no void-> it is converted to undefined.
PrintResult(add(4,18));


// // Now we should define how let combined values will store our add function .
// // () => number : indicates a function which takes no parameter and returns a number
let combinedValue: (a:number,b:number) => number;

combinedValue = add;
// combinedValue = PrintResult; // takes only one parameter (a:number)=> void
// // combinedValue = 5;

console.log(combinedValue(8,12));



// callbacks
function addandHandle(n1 : number,n2 : number,cb : (num : number) => void){
    let result = n1 + n2;
    cb(result);
}

// without fat arrow
addandHandle(16,14,function(result){
    console.log(result);
})


addandHandle(887,28,(result)=>{
    console.log(result);
})



