// unknown type
// can be assigned any value but it is different from any type because of restriction on unknown as it cannot be assigned to other variable of assigned data type whereas any can be assigned

let myInput : unknown;
let myName : string;

myInput = 60;
myInput = 'naman is Palindrome'
// myName = myInput; // Type 'unknown' is not assignable to type 'string
if(typeof myInput === 'string'){
    myName = myInput;
}


function genrateError(message : string,code: number) : never{
    throw  {message: message, errorCode : code};
}

genrateError("An error occured!",500);