// tsc.cmd app.ts -w : it will run the whole file automatically but for project we have do something else
// for entire project
// Step -1 -> tsc.cmd --init
// Step -2 -> tsc.cmd it will run all the type script files
// Step -3 -> exclude those ts files from json which are not required to be run.
// Step -4 -> we can include those ts files only needed to be executed -> includes whole directory
// Step -5 -> we can include just files by using files this for smaller project.

const userName = 'Priyesh Pandey';
console.log(userName);