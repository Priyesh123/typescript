// let person : {
//     name : string;
//     age : number;
//     hobbies : string[];
//     role : [number , string];
// } = {
//     name : 'Priyesh',
//     age : 21,
//     hobbies : ['Reading',"Travelling","sports"],
//     role : [2 , 'SDE'] // tuple can take only two values of different types . Fixed length
// }

enum Role {ADMIN , READ_ONLY , AUTHOR}
let person = {
    name : 'Priyesh',
    age : 21,
    hobbies : ['Reading',"Travelling","sports"],
    role : Role.AUTHOR
}


// let array : string[];
// // array = ['lemon','rice','cost',100]; // Error only string should be entered here

let array : any[];
array = ['lemon','rice','cost is',100,true,'100']; 

let filteredarry : string[];
filteredarry = array.filter(function(val){
    if(typeof val === 'string')
    {
        return val;
    }
})

let str = '';
filteredarry.map((val)=>{
    str += `${val} `;
})
console.log(str);

console.log(person.name);
// person.role.map(function(rle){
//     console.log(`${rle}`)
// });
// person.role.push('Intern') // won't show error here as it exception of tuple.

// person.role[1] = 10; error
// person.role = [1,'SDE','Intern'] error -> intern extra

for(let hobby of person.hobbies)
{
    console.log(hobby.toUpperCase());
    // person.hobbies.map(function(hobby,fav){
    //     console.log(` My ${fav+1} hobby is ${hobby}`)
    // });
}

if(person.role == Role.AUTHOR)
{
    console.log('is Admin');
}