// type alias
type Combinable = number | string;
type ConversionDescriptor = 'as-number' | 'as-text';


// Union Types
function combine(n1 : Combinable,n2: Combinable, resultConversion : ConversionDescriptor){
    let result : number | string;
    if((typeof n1 === 'number' && typeof n2 === 'number' && resultConversion == 'as-number') || resultConversion === 'as-number')
    {
        result =  +n1 + +n2;
    }
    else{
        result = n1.toString() + n2.toString();
    }
    return result;
}



// Literal types
let ans1 = combine(60,7,'as-number');
console.log(ans1);

let ans2 = combine(30,26,'as-number');
console.log(ans2);

let ans4 = combine(30,26,'as-text');
console.log(ans4);

let ans3 = combine('Priyesh','Pandey','as-text');
console.log(ans3);

let ans5 = combine('Priyesh','Pandey','as-number');
console.log(ans5);